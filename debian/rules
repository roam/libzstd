#!/usr/bin/make -f

export DH_VERBOSE=1

arch ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
export DEB_BUILD_MAINT_OPTIONS=hardening=+all

# Upstream's makefiles will respect PREFIX, and this means we avoid a patch
export PREFIX=/usr

HELP2MAN = help2man --no-info --help-option="'-h'" --no-discard-stderr
mandir := $(CURDIR)/debian/zstd/usr/share/man/man1

%:
	dh $@ --buildsystem=cmake

DH_AUTO_CONFIGURE_OPTS := \
	-DCMAKE_LIBRARY_ARCHITECTURE=${arch} \
	-DZSTD_BUILD_CONTRIB:BOOL=ON \

ifneq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	DH_AUTO_CONFIGURE_OPTS += -DBUILD_TESTING:BOOL=OFF
else
	DH_AUTO_CONFIGURE_OPTS += -DBUILD_TESTING:BOOL=ON
endif

override_dh_auto_configure:
	dh_auto_configure -- \
		$(DH_AUTO_CONFIGURE_OPTS) \
		"$(CURDIR)/build/cmake"

execute_after_dh_install:
	cp -r debian/libzstd1/usr debian/libzstd1-udeb/

override_dh_makeshlibs:
	dh_makeshlibs -plibzstd1 -V'libzstd1 (>= 1.5.2)' --add-udeb=libzstd1-udeb

execute_after_dh_installman:
	cp $(mandir)/zstd.1 $(mandir)/zstdmt.1
	$(HELP2MAN) --name='parallelized Zstandard compression, a la pigz' obj-$(arch)/contrib/pzstd/pzstd \
	| perl -pe 's/(\(de\)compression\s\(default:)(\d+)(\))/$$1 All$$3/g' >$(mandir)/pzstd.1
